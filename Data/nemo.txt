# Exemple
# ./parseAll ../Data/nemo.txt
# Replace the <source> and the <destination> folders by your own local folders
#
#Python /Users/philippe/git/nemo/cfdrl						/Users/philippe/temp/nemo/cfdrl
#None 	/Users/philippe/git/nemo/cfdrl-therlib				/Users/philippe/temp/nemo/cfdrl-therlib
#None 	/Users/philippe/git/nemo/cluster-additions			/Users/philippe/temp/nemo/cluster-additions
#None 	/Users/philippe/git/nemo/flux-cd					/Users/philippe/temp/nemo/flux-cd
#Docker /Users/philippe/git/nemo/fredy						/Users/philippe/temp/nemo/fredy
#None 	/Users/philippe/git/nemo/helmchart					/Users/philippe/temp/nemo/helmchart
#Python /Users/philippe/git/nemo/intent-api					/Users/philippe/temp/nemo/intent-api
#None 	/Users/philippe/git/nemo/intent-based-system		/Users/philippe/temp/nemo/intent-based-system
#None 	/Users/philippe/git/nemo/keycloak					/Users/philippe/temp/nemo/keycloak
Maven 	/Users/philippe/git/nemo/message-broker				/Users/philippe/temp/nemo/message-broker
Go 		/Users/philippe/git/nemo/meta-orchestrator/orchestration-engine			/Users/philippe/temp/nemo/meta-orchestrator/orchestration-engine
#Python /Users/philippe/git/nemo/moca/event-server/app		/Users/philippe/temp/nemo/moca/event-server/app
Go 		/Users/philippe/git/nemo/multi-domain-l2s-m/l2sm-api-resources			/Users/philippe/temp/nemo/multi-domain-l2s-m/l2sm-api-resources
#None 	/Users/philippe/git/nemo/nemo						/Users/philippe/temp/nemo/nemo
#Docker /Users/philippe/git/nemo/nemo-access-control		/Users/philippe/temp/nemo/nemo-access-control
#None 	/Users/philippe/git/nemo/nemo-docs					/Users/philippe/temp/nemo/nemo-docs
Nodes 	/Users/philippe/git/nemo/nemo-lcm-ui				/Users/philippe/temp/nemo/nemo-lcm-ui
#None 	/Users/philippe/git/nemo/network-exposure-module	/Users/philippe/temp/nemo/network-exposure-module
#None 	/Users/philippe/git/nemo/rabbitmq					/Users/philippe/temp/nemo/rabbitmq
#Python /Users/philippe/git/nemo/sla-registry				/Users/philippe/temp/nemo/sla-registry
#Docker /Users/philippe/git/nemo/underlay-network-probe		/Users/philippe/temp/nemo/underlay-network-probe
#Python /Users/philippe/git/nemo/workload-provisioning		/Users/philippe/temp/nemo/workload-provisioning