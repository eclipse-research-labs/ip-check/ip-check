#!/bin/bash
#*******************************************************************************
# Copyright (c) 2024 The Eclipse Foundation
#
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     phkrief - initial script
#******************************************************************************* 
#
#--------------------------------------------------------------
# Runs a dependency analysis on each SmartCLIDE repo
#--------------------------------------------------------------
#
#--------------------------------------------------------------
# Runs the Eclipse DASH dependency analysis on all SmartCLIDE repositories
#		$1 	: The Path to the file containing the parameters
#			Format of each line in the file:
#
#			<type>	<source folder>		<destination folder>
#
#			with <type>: "Maven" | "Nodes" | "Yarn" | "Go"
#		$2	: The Path to the folder where the git repos are cloned
#		$3 	: The Path to the folder where the results of the analysis will be stored
#
# Example: ./parseAll ../Data/nemo.txt /Users/philippe/git/nemo /Users/philippe/temp/nemo
#---------------------------------------------------------------
# Check if the file exists
if [ ! -f "$1" ]; then
	echo "File $1 not found!"
	echo "Syntax: parseAll.sh <parameters.txt>"
	echo "with  <parameters.txt> listing the parameters for dashAnalysis.sh:"
	echo "<type>	<source folder>		<destination folder>"
	echo "with: <type> = Maven | Nodes |  Yarn | Go"
	exit 1
fi

# Grab arguments
PARAMETERS=$1 

#Read the file line by line
while IFS= read -r LINE; do
	# Skip lines that start with a #
	if [[ $LINE =~ ^# ]]; then
		continue
	fi
  	# Call the analysis with the parameters from the current line
	./dashAnalysis.sh $LINE
done  < "$PARAMETERS"
